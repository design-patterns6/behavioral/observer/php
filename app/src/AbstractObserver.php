<?php

declare(strict_types=1);

namespace Observer;

abstract class AbstractObserver
{
    abstract public function update(AbstractSubject $subjectIn): void;
}

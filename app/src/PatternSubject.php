<?php

declare(strict_types=1);

namespace Observer;

class PatternSubject extends AbstractSubject
{
    private array $observers = [];
    private string $favorites;

    public function attach(AbstractObserver $observerIn): void
    {
        $this->observers[] = $observerIn;
    }

    public function detach(AbstractObserver $observerIn): void
    {
        foreach ($this->observers as $key => $val) {
            if ($val === $observerIn) {
                unset($this->observers[$key]);
            }
        }
    }

    public function notify(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    public function updateFavorites(string $newFavorites): void
    {
        $this->favorites = $newFavorites;
        $this->notify();
    }

    public function getFavorites(): string
    {
        return $this->favorites;
    }
}

<?php

declare(strict_types=1);

namespace Observer;

class PatternObserver extends AbstractObserver
{
    public function update(AbstractSubject $subjectIn): void
    {
        $this->writeln('*IN PATTERN OBSERVER - NEW PATTERN GOSSIP ALERT*');
        $this->writeln(' new favorite patterns: '.$subjectIn->getFavorites());
        $this->writeln('*IN PATTERN OBSERVER - PATTERN GOSSIP ALERT OVER*');
    }

    private function writeln(string $lineIn): void
    {
        echo $lineIn.'<br/>';
    }
}

<?php

declare(strict_types=1);

namespace Observer;

abstract class AbstractSubject
{
    abstract public function attach(AbstractObserver $observerIn): void;

    abstract public function detach(AbstractObserver $observerIn): void;

    abstract public function notify(): void;
}

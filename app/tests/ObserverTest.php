<?php

declare(strict_types=1);

namespace Observer\Tests;

use Observer\PatternObserver;
use Observer\PatternSubject;
use PHPUnit\Framework\TestCase;

class ObserverTest extends TestCase
{
    private const UPDATED = 'updated';
    private const NOT_UPDATED = 'not updated';

    public function testPatternSubjectShouldNotifyWhenObserverIsAttached(): void
    {
        // GIVEN
        $patternGossiper = new PatternSubject();
        $patternGossipFan = new PatternObserver();
        // WHEN
        $patternGossiper->attach($patternGossipFan);
        $patternGossiper->updateFavorites(self::UPDATED);
        // THEN
        self::assertStringContainsString(needle: self::UPDATED, haystack: self::getActualOutput());
    }

    public function testPatternSubjectShouldNotifyOnlyWhenObserverIsAttached(): void
    {
        // GIVEN
        $patternGossiper = new PatternSubject();
        $patternGossipFan = new PatternObserver();
        $patternGossiper->attach($patternGossipFan);
        $patternGossiper->updateFavorites(self::UPDATED);
        $patternGossiper->detach($patternGossipFan);
        // WHEN
        $patternGossiper->updateFavorites(self::NOT_UPDATED);
        // THEN
        self::assertStringNotContainsString(needle: self::NOT_UPDATED, haystack: self::getActualOutput());
    }
}
